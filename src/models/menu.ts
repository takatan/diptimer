import { pipe } from 'effect';
import { type Duration, seconds } from 'effect/Duration';
import { flatten, map, range } from 'effect/ReadonlyArray';

export interface IMenuList {
  entries: IMenuEntry[];
}

export interface IMenuEntry {
  name: string;
  timers: ITimerEntry[];
}

export interface ITimerEntry {
  title: string;
  duration: Duration;
}

export const defaultMenuList: IMenuList = {
  entries: flatten([
    [
      {
        name: 'ディプロマシー',
        timers: [
          { title: '外交フェイズ', duration: seconds(15 * 60) },
          { title: '命令記述フェイズ', duration: seconds(5 * 60) },
          { title: '命令解決フェイズ', duration: seconds(10 * 60) },
        ],
      },
      {
        name: 'テスト',
        timers: [
          { title: 'A', duration: seconds(2) },
          { title: 'B', duration: seconds(4) },
          { title: 'C', duration: seconds(3) },
        ],
      },
      {
        name: 'テスト2',
        timers: [{ title: 'A', duration: seconds(2) }],
      },
    ],
    pipe(
      range(1, 15),
      map((i: number) => ({
        name: `${i}分`,
        timers: [{ title: `${i}分`, duration: seconds(i * 60) }],
      })),
    ),
  ]),
};
