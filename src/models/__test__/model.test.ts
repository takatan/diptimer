import { millis } from 'effect/Duration';
import { displayFormat } from '../time';

describe('time', () => {
  test('displayFormat', () => {
    expect(displayFormat(millis(-1))).toBe('00:00:00');
    expect(displayFormat(millis(1001))).toBe('00:00:02');
    expect(displayFormat(millis(1000))).toBe('00:00:01');
    expect(displayFormat(millis(999))).toBe('00:00:01');
    expect(displayFormat(millis(1000 * 60))).toBe('00:01:00');
    expect(displayFormat(millis(1000 * 60 - 1))).toBe('00:01:00');
    expect(displayFormat(millis(1000 * 60 + 1))).toBe('00:01:01');
    expect(displayFormat(millis(1000 * 60 * 60))).toBe('01:00:00');
    expect(displayFormat(millis(1000 * 60 * 60 - 1))).toBe('01:00:00');
    expect(displayFormat(millis(1000 * 60 * 60 + 1))).toBe('01:00:01');
  });
});
