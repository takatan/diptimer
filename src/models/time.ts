import { pipe } from 'effect';
import { type Duration, seconds, toSeconds } from 'effect/Duration';
import { filter, flatten, map, range, reverse } from 'effect/ReadonlyArray';
import { printf } from 'fast-printf';

export const displayFormat = (r: Duration): string => {
  const sec = Math.ceil(toSeconds(r));
  return sec <= 0
    ? '00:00:00'
    : printf(
        '%02d:%02d:%02d',
        sec / 3600,
        Math.floor(sec % 3600) / 60,
        Math.floor(sec) % 60,
      );
};
export const speechString = (r: Duration): string => {
  const left = Math.ceil(toSeconds(r));
  if (left < 10) {
    return left.toString();
  }

  const hour = Math.floor(left / 3600);
  const min = Math.floor((left % 3600) / 60);
  const sec = Math.floor(left % 60);
  return `残り${hour > 0 ? `{hour}時間` : ''}
        ${min > 0 ? `${min}分` : ''}
        ${sec > 0 ? `${sec}秒` : ''}
        です`;
};

export const getCheckPoints = (limit: Duration): Duration[] => {
  return pipe(
    [
      range(1, 5),
      map((i: number) => i * 10)(range(1, 5)),
      map((i: number) => i * 60)(range(1, 15)),
    ],
    flatten,
    filter((e: number) => e < toSeconds(limit)),
    map(seconds),
    reverse,
  );
};
