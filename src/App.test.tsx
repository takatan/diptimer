import { render } from '@testing-library/react';
import * as React from 'react';
import App from './App';

it('renders without crashing', () => {
  const pauseStub = jest
    .spyOn(window.HTMLMediaElement.prototype, 'pause')
    .mockImplementation(() => {});

  expect(() => {
    render(<App />);
  }).not.toThrow();

  pauseStub.mockRestore();
});
