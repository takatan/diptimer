import { pipe } from 'effect';
import { type Duration, millis, zero } from 'effect/Duration';
import { constant } from 'effect/Function';
import * as O from 'effect/Option';
import { none, type Option } from 'effect/Option';
import { get } from 'effect/ReadonlyArray';
import { evolve } from 'effect/Struct';
import { atom, selector } from 'recoil';
import { calcRemainMilliSecond } from '../components/ControlButtons';
import {
  defaultMenuList,
  type IMenuEntry,
  type ITimerEntry,
} from '../models/menu';
import { displayFormat, getCheckPoints } from '../models/time';

export type WatchState = 'STANDBY' | 'RUNNING' | 'SUSPEND' | 'FINISHED';

export interface RootState {
  menuIndex: number;
  timerIndex: number;
  watchState: WatchState;
  left: Duration;
  checkPoints: Duration[];
  startedAt: Option<Date>;
  timeoutId: Option<number>;
}

export const displaySelector = selector({
  key: 'display',
  get: ({ get }) => {
    const root = get(rootState);
    return displayFormat(millis(calcRemainMilliSecond(root)));
  },
});

export const getTimerEntry = (menu: number, timer: number): ITimerEntry => {
  return pipe(
    defaultMenuList.entries,
    get(menu),
    O.flatMap((a: IMenuEntry) => get(a.timers, timer)),
    O.getOrThrow,
  );
};

export const getNextTimer = (self: RootState): Option<ITimerEntry> =>
  pipe(
    defaultMenuList.entries,
    get(self.menuIndex),
    O.flatMap((a: IMenuEntry) => get(a.timers, self.timerIndex + 1)),
  );
export const reset = (root: RootState): RootState => {
  O.map(root.timeoutId, window.clearTimeout);
  const duration = getTimerEntry(root.menuIndex, 0).duration;
  return pipe(
    root,
    evolve({
      timerIndex: constant(0),
      watchState: constant('STANDBY' as const),
      checkPoints: constant(getCheckPoints(duration)),
      left: constant(duration),
      startedAt: none<Date>,
    }),
  );
};

export const setMenuIndex =
  (index: number) =>
  (root: RootState): RootState => {
    return pipe(
      root,
      evolve({
        menuIndex: constant(index),
      }),
      reset,
    );
  };

export const rootState = atom<RootState>({
  key: 'root',
  default: reset({
    menuIndex: 0,
    timerIndex: 0,
    watchState: 'STANDBY',
    left: zero,
    checkPoints: [],
    startedAt: none(),
    timeoutId: none(),
  }),
});
