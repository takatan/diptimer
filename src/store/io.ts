import NoSleep from 'nosleep.js';

export const noSleep = new NoSleep();

export const runHandler = (handler: () => void): number => {
  return window.setTimeout(handler, 1000);
};

export const speak = (text: string): void => {
  const synthesis = new SpeechSynthesisUtterance(text);
  synthesis.lang = 'ja-JP';
  synthesis.rate = 1.0;
  speechSynthesis.speak(synthesis);
};
