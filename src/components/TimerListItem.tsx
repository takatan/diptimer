import { Timer, TimerOff } from '@mui/icons-material';
import {
  IconButton,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
} from '@mui/material';
import { pipe } from 'effect';
import { toSeconds } from 'effect/Duration';
import { constant } from 'effect/Function';
import * as Option from 'effect/Option';
import { get, map } from 'effect/ReadonlyArray';
import { printf } from 'fast-printf';
import * as React from 'react';
import { useRecoilValue } from 'recoil';
import {
  defaultMenuList,
  type IMenuEntry,
  type ITimerEntry,
} from '../models/menu';
import { rootState } from '../store/root';

export const TimerListItem: React.FC = () => {
  const root = useRecoilValue(rootState);
  const menuIndex = root.menuIndex;
  const timerIndex = root.timerIndex;
  return pipe(
    defaultMenuList.entries,
    get(menuIndex),
    Option.map((a: IMenuEntry) => (
      <List key={menuIndex}>
        {map((e: ITimerEntry, i: number) => (
          <ListItem
            className={i === timerIndex ? 'timer-list-selected' : 'timer-list'}
            key={i}
          >
            <ListItemText>
              {printf(
                '%s %d:%02d',
                e.title,
                Math.floor(toSeconds(e.duration) / 60),
                toSeconds(e.duration) % 60,
              )}
            </ListItemText>
            <ListItemSecondaryAction>
              <IconButton aria-label="Delete">
                {i === timerIndex ? <Timer /> : <TimerOff />}
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))(a.timers)}
      </List>
    )),
    Option.getOrElse(constant(<></>)),
  );
};
