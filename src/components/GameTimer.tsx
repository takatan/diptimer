import { Divider, List } from '@mui/material';
import * as React from 'react';
import { ControlButtonsComponent } from './ControlButtons';
import { SelectMenu } from './SelectMenu';
import { TimeDisplay } from './TimeDisplay';
import { TimerListItem } from './TimerListItem';

export const GameTimerApp: React.FC = () => {
  return (
    <div>
      <SelectMenu />
      <TimerListItem />
      <List>
        <Divider />
        <TimeDisplay />
        <ControlButtonsComponent />
      </List>
    </div>
  );
};
