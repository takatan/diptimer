import { MenuItem, Select } from '@mui/material';
import { pipe } from 'effect';
import { map } from 'effect/ReadonlyArray';
import * as React from 'react';
import { useRecoilState } from 'recoil';
import { defaultMenuList, type IMenuEntry } from '../models/menu';
import { rootState, setMenuIndex } from '../store/root';

export const SelectMenu: React.FC = () => {
  const [root, setRoot] = useRecoilState(rootState);
  const menuIndex = root.menuIndex;
  return (
    <Select
      value={menuIndex}
      onChange={(ev): void => {
        setRoot(setMenuIndex(ev.target.value as number));
      }}
    >
      {pipe(
        defaultMenuList.entries,
        map((e: IMenuEntry) => e.name),
        map((n: string, i: number) => (
          <MenuItem value={i} key={i}>
            {n}
          </MenuItem>
        )),
      )}
    </Select>
  );
};
