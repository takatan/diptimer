import { Pause, PlayArrow } from '@mui/icons-material';
import { Button } from '@mui/material';
import { pipe } from 'effect';
import {
  type Duration,
  millis,
  toMillis,
  toSeconds,
  zero,
} from 'effect/Duration';
import { constant } from 'effect/Function';
import * as O from 'effect/Option';
import { exists, getOrElse, match, none, some } from 'effect/Option';
import { head, tail } from 'effect/ReadonlyArray';
import { evolve } from 'effect/Struct';
import * as React from 'react';
import { useEffect } from 'react';
import { useRecoilState } from 'recoil';
import type { ITimerEntry } from '../models/menu';
import { getCheckPoints, speechString } from '../models/time';
import { noSleep, speak } from '../store/io';
import {
  getNextTimer,
  getTimerEntry,
  reset,
  type RootState,
  rootState,
  type WatchState,
} from '../store/root';

export const calcRemainMilliSecond = (root: RootState): number =>
  pipe(
    root.startedAt,
    O.map((startedAt) => new Date().getTime() - startedAt.getTime()),
    O.getOrElse(constant(0)),
    (x: number) => toMillis(root.left) - x,
    toMillis,
  );
export const ControlButtonsComponent: React.FC = () => {
  const [root, setRoot] = useRecoilState(rootState);
  useEffect(() => {
    // noinspection BlockStatementJS
    switch (root.watchState) {
      case 'RUNNING': {
        noSleep.enable().then().catch(console.error);
        const intervalId = window.setInterval(() => {
          const remainMilliSecond = calcRemainMilliSecond(root);
          if (remainMilliSecond <= 0) {
            pipe(
              root,
              getNextTimer,
              match({
                onNone: () => {
                  noSleep.disable();
                  speak('終了です。');
                  setRoot(
                    evolve({
                      left: constant(zero),
                      watchState: constant('FINISHED' as const),
                    }),
                  );
                },
                onSome: (e: ITimerEntry) => {
                  speak(`${e.title}です`);
                  setRoot(
                    evolve({
                      timerIndex: (n) => n + 1,
                      left: constant(e.duration),
                      checkPoints: constant(getCheckPoints(e.duration)),
                      startedAt: () => some(new Date()),
                    }),
                  );
                },
              }),
            );
            return;
          }

          const hit = pipe(
            root.checkPoints,
            head,
            exists(
              (second: Duration) =>
                toSeconds(remainMilliSecond) < toSeconds(second),
            ),
          );
          if (hit) {
            speak(speechString(millis(remainMilliSecond)));
          }
          setRoot(
            evolve({
              checkPoints: (cs): Duration[] => {
                return hit ? pipe(cs, tail, getOrElse(constant([]))) : cs;
              },
            }),
          );
        }, 333);
        return () => {
          window.clearInterval(intervalId);
        };
      }
      case 'STANDBY':
        noSleep.disable();
        return () => {};
      case 'SUSPEND':
        noSleep.disable();
        return () => {};
      case 'FINISHED':
        noSleep.disable();
        return () => {};
      default: {
        const _: never = root.watchState;
        throw new Error(_);
      }
    }
  }, [root.watchState, root.left, root.checkPoints]);
  const watchState = root.watchState;

  const onPlayClick = (): void => {
    const { menuIndex, timerIndex } = root;
    if (watchState === 'STANDBY') {
      const entry = getTimerEntry(menuIndex, timerIndex);
      speak(`${entry.title}です`);
      setRoot(
        evolve({
          watchState: constant('RUNNING' as const),
          left: constant(entry.duration),
          startedAt: (e) => some(new Date()),
        }),
      );
    } else {
      setRoot(
        evolve({
          watchState: constant('RUNNING' as const),
          startedAt: (e) => some(new Date()),
        }),
      );
    }
  };

  const onPauseClick = (): void => {
    noSleep.disable();
    O.map(root.timeoutId, window.clearTimeout);
    setRoot(
      evolve({
        left: constant(millis(calcRemainMilliSecond(root))),
        watchState: constant('SUSPEND' as const),
        startedAt: none<Date>,
        timeoutId: none<number>,
      }),
    );
  };

  const StartPauseButton = ({
    watchState,
  }: {
    watchState: WatchState;
  }): React.ReactElement => {
    switch (watchState) {
      case 'RUNNING':
        return (
          <Button variant="contained" className="button" onClick={onPauseClick}>
            <Pause /> PAUSE
          </Button>
        );
      case 'STANDBY':
      case 'SUSPEND':
        return (
          <Button variant="contained" className="button" onClick={onPlayClick}>
            <PlayArrow /> START
          </Button>
        );
      case 'FINISHED':
        return (
          <Button variant="contained" className="button" disabled={true}>
            <Pause />
          </Button>
        );
      default:
        return <PlayArrow />;
    }
  };

  return (
    <div className="control-buttons">
      <StartPauseButton watchState={watchState} />
      <Button
        variant="contained"
        className="button"
        color="secondary"
        onClick={() => {
          noSleep.disable();
          setRoot(reset);
        }}
      >
        Reset
      </Button>
    </div>
  );
};
