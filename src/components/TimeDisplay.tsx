import * as React from 'react';
import { useRecoilValue } from 'recoil';
import { displaySelector, rootState } from '../store/root';

export const TimeDisplay: React.FC = () => {
  const root = useRecoilValue(rootState);
  const display = useRecoilValue(displaySelector);
  const watchState = root.watchState;

  return (
    <div
      className={`time-display ${watchState === 'FINISHED' ? 'finished' : ''}`}
    >
      <code>{display}</code>
    </div>
  );
};
