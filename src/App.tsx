import { createTheme } from '@mui/material';
import { ThemeProvider } from '@mui/styles';
import * as React from 'react';
import { RecoilRoot } from 'recoil';
import './App.css';
import { GameTimerApp } from './components/GameTimer';

const theme = createTheme({});

const App = (): React.ReactElement => (
  <RecoilRoot>
    <ThemeProvider theme={theme}>
      <GameTimerApp />
    </ThemeProvider>
  </RecoilRoot>
);

export default App;
